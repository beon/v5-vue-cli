

const require = {
    context(url, deep, reg) {
        console.log(url, deep, reg);
    }
};

const requireComponent = require.context(
    './components/common/form',
    false,
    /\.vue$/
);
requireComponent.keys().forEach((fileName) => {
    const componentConfig = requireComponent(fileName);
    console.log(componentConfig);
});
