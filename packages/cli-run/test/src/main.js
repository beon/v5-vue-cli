import Vue from 'vue';
import App from './App.vue';
import './assets/css/global.scss';
import '../';
import 'jquery';
import 'assets/css/demo.css';
import 'assets/css/element-variables.scss';

Vue.config.productionTip = false;

new Vue({
    render: (h) => h(App),
}).$mount('#app');
