# cli-run (vue2快速开发脚手架)

## 介绍
> 本项目为最大意义在于将webpack项目可以直接利用esmodule来达到快速运行开发

## 架构
> 项目整体主要利用了es-build和es-module-lexer来进行包和引入的处理

## 文档
> https://juejin.cn/post/6975380991762235422

## 安装
> npm i @seeyon-v5-vue/cli-run -g

## 使用
暂时只有一条指令为 
```shell
v5-run
```

> 使用的关键点在于配置文件, 在项目根目录下创建配置文件v5-run.config.js,下面为配置文件的d.ts文件
```typescript
declare namespace Config{
	interface proxy{
		[url: string]: proxyParams
	}

	interface proxyParams {
		target: string,
		host?: string,
		headers?: {[key: string]: string}
		changOrigin?: boolean
	}

	interface server{
		proxy?: proxy
		http2?: boolean
		enter?: string[]
	}

	interface svgLoader{
		path: string,
		symbolId: string
	}

	interface styleLoader {
		[loadStyle: string]: string[]
	}

	interface global {
		[url: string]: string | string[]
	}

	interface config {
		svgLoader?: svgLoader
		styleLoader?: styleLoader
		global?: global
	}

	interface Config{
		config?: config
		server?: server
	}
}
```

## 说明
> 本项目任然在开发状态，现阶段为初版（现在没有热更新，没有热更新，没有热更新）
> 
> 本项目的三方依赖处理代码借鉴了vite，所以会有少量雷同，但是目标不一样，这是为了针对vue2的现有webpack项目能够利用上类似vite的快速开发，以及后期有类似vue单组件快速开发模式的node_modules依赖集合管理

## 作者
> 本项目由 [Beon](https://juejin.cn/user/1662117312988695) 全程开发
