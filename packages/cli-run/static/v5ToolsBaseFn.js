window.process = {
    env: {
        NODE_ENV: 'development',
        ENV: 'development',
        VUE_APP_BASE_API: '/dev-api'
    }
};
window.v5ToolCreateSvg = function(svg) {
    const svgDom = document.createElement('svg');
    document.body.prepend(svgDom);
    svgDom.outerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="position: absolute; width: 0; height: 0" id="__SVG_SPRITE_NODE__">${svg}</svg>`;
    // svgDom.style.display = 'none';
};
