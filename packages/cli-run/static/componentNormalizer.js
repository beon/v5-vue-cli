export default function normalizeComponent (
    scriptExports,
    render,
    staticRenderFns,
    functionalTemplate,
    injectStyles,
    scopeId,
    moduleIdentifier, /* server only */
    shadowMode /* vue-cli only */
) {
    // Vue.extend constructor export interop
    var options = typeof scriptExports === 'function'
        ? scriptExports.options
        : scriptExports;

    // render functions
    if (render) {
        options.render = render;
        options.staticRenderFns = staticRenderFns;
        options._compiled = true;
    }

    // functional template
    if (functionalTemplate) {
        options.functional = true;
    }

    // scopedId
    if (scopeId) {
        options._scopeId = 'data-v-' + scopeId;
    }

    var hook;
    if (moduleIdentifier) { // server build
        hook = function (context) {
            context =
          context || // cached call
          (this.$vnode && this.$vnode.ssrContext) || // stateful
          (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            if (injectStyles) {
                injectStyles.call(this, context);
            }
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        options._ssrRegister = hook;
    } else if (injectStyles) {
        hook = shadowMode
            ? function () {
                injectStyles.call(
                    this,
                    (options.functional ? this.parent : this).$root.$options.shadowRoot
                );
            }
            : injectStyles;
    }

    if (hook) {
        if (options.functional) {
            options._injectStyles = hook;
            var originalRender = options.render;
            options.render = function renderWithStyleInjection (h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        } else {
            var existing = options.beforeCreate;
            options.beforeCreate = existing
                ? [].concat(existing, hook)
                : [hook];
        }
    }

    return {
        exports: scriptExports,
        options: options
    };
}

export function RequireContext(obj) {
    if (this instanceof RequireContext){
        this.map = obj;
        const backFn = (url) => ({
            default: this.map[url].content
        });
        backFn.keys = () => Object.keys(this.map).map((item) => this.map[item].fileName);
        return backFn;
    }
    console.warn('请不要主动调用脚手架内置方法，以免出现打包不一致问题') ;
}
