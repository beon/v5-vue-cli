/**
 * @Description: vue文件入口处理
 * @author Beon
 * @date 2021/4/9
*/
import {rewriteDefault} from '../utils/rewriteDefault';
import * as esbuild from 'esbuild';

export default class TemplateDealMain {
    // static defaultExportRE = /export\s+default\s+class\s+([\w$]+)/

    /**
	 * vue文件script主入口处理
	 * @param ctx
	 * @param descriptor
	 */
    static async transformMain(ctx, descriptor): Promise<void> {
	    let code = '';
	    ctx.type = 'js';
	    code += this.templateRequest(ctx, descriptor);
	    code += await this.scriptCreate(descriptor);

	    const { styleCode, scoped } = this.styleRequests(ctx, descriptor);
	    code += styleCode;

	    code += this.componentCreate(descriptor, scoped);
	    code += '\nexport default component.exports';
	    ctx.body = code;
    }

    /**
	 * 转换template为import
	 * @param ctx
	 * @param descriptor
	 * @returns {string}
	 */
    private static templateRequest(ctx, descriptor) {
	    if (!descriptor.template) {return 'const render = null,\nstaticRenderFns = null;';}

	    const templateRequest = ctx.path + '?vue&type=template';
	    return `\nimport { render,staticRenderFns } from ${JSON.stringify(
	        templateRequest
	    )}\n`;
    }

    /**
	 * 创建script内容
	 * @param descriptor
	 * @returns {string | {scriptCode: string}}
	 */
    private static async scriptCreate(descriptor) {
	    const { script } = descriptor;
	    let scriptCode = 'const script = {}';
	    if (!script) {
	        return scriptCode;
	    }
        script.content = await this.dealJsx(script.content);
	    return rewriteDefault(script.content, 'script');
    }

    /**
	 * 利用esbuild进行代码esm转换
	 * @returns {Promise<TransformResult>}
	 * @param body
	 */
    private static async dealJsx(body) {
        return (await esbuild.transform(body, {
            jsxFactory: 'h',
            loader: 'jsx',
            treeShaking: 'ignore-annotations'
        })).code;
    }

    /**
	 * 转换style为import
	 * @param ctx
	 * @param descriptor
	 * @returns {{scoped: boolean, styleCode: any}}
	 */
    private static styleRequests(ctx, descriptor) {
	    let scoped = false;
	    return {
	    	styleCode: descriptor.styles.reduce((all, item, index) => {
	            if (item.scoped) {scoped = true;}
	            all += `\nimport ${JSON.stringify(ctx.path + `?vue&type=style&index=${index}`)}`;
	            return all;
	        }, ''),
	        scoped
	    };
    }

    /**
	 * vue实例构造器代码
	 * @param descriptor
	 * @param scoped
	 * @returns {string}
	 */
    private static componentCreate(descriptor, scoped) {
	    const hasFunctional =
			descriptor.template && descriptor.template.attrs.functional;

	    return `
/* normalize component */
import normalizer from "vueComponentNormalizer"
var component = normalizer(
  script,
  render,
  staticRenderFns,
  ${hasFunctional ? 'true' : 'false'},
  null,
  ${scoped ? JSON.stringify(descriptor.id) : 'null'},
  null,
  null
)\n`.trim();
    }
}
