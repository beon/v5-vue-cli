/**
 * @Description: 样式模板预处理
 * @author Beon
 * @date 2021/4/9
*/
import { compileStyle } from '@vue/component-compiler-utils';
import BaseStyleLoad from '../utils/baseStyleLoad';

export interface StyleCode {
    code: string,
    map: any
}

export default class TemplateDealStyle{

    /**
     * 样式模板处理入口
     * @param ctx
     * @param descriptor
     * @returns {Promise<void>}
     */
    static async transformStyle(
        ctx,
        descriptor
    ): Promise<void> {
        ctx.type = 'js';
        ctx.body = BaseStyleLoad.createStyleDom(await this.dealStyle(ctx, descriptor));
    }

    /**
     * 样式处理真正方法
     * @param ctx
     * @param descriptor
     * @private
     */
    private static async dealStyle(ctx, descriptor) {
        const block = descriptor.styles[ctx.query.index];
        descriptor.styles[ctx.query.index].lang && (block.content = await BaseStyleLoad.styleLoad[descriptor.styles[ctx.query.index].lang](block.content, ctx));
        const result = compileStyle({
            source: block.content,
            filename: '',
            id: `data-v-${descriptor.id}`,
            scoped: !!block.attrs.scoped,
            trim: true,
        });

        if (result.errors.length) {
            return null;
        }

        return result.code;
    }
}
