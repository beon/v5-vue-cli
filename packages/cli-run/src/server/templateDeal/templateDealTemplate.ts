/**
 * @Description: 处理模板template
 * @author Beon
 * @date 2021/4/9
*/
import {compile} from 'vue-template-compiler';
import * as transpile from 'vue-template-es2015-compiler';
import * as path from 'path';

interface AssetURLOptions {
    [name: string]: string | string[]
}

interface Attr {
    name: string
    value: string
}

interface ASTNode {
    tag: string
    attrs: Attr[]
}

export default class TemplateDealTemplate{
    // 默认需要特殊转换路径的标签
    static defaultOptions: AssetURLOptions = {
        audio: 'src',
        video: ['src', 'poster'],
        source: 'src',
        img: 'src',
        image: ['xlink:href', 'href'],
        use: ['xlink:href', 'href'],
    }

    /**
     * 转换template入口
     * @param ctx
     * @param descriptor
     */
    static transformTemplate(ctx, descriptor) {
        ctx.type = 'js';
        let content = descriptor.template.content;
        const {render, staticRenderFns} = compile(content, {
            modules: [this.assetUrlsModule(ctx)]
        });
        const toFunction = (codeParam) => `function () {${codeParam}}`;
        let code = transpile(`var __render__ = ${toFunction(render)}\n` +
			`var __staticRenderFns__ = [${staticRenderFns.map(toFunction)}]`, { transforms: { stripWithFunctional: false } }) + '\n';
        code = code.replace(/\s__(render|staticRenderFns)__\s/g, ' $1 ');
        code += '\nexport { render, staticRenderFns }';
        ctx.body = code;
    }

    /**
     * compile插件
     * @param ctx
     * @returns {{postTransformNode: (node: ASTNode) => void}}
     */
    private static assetUrlsModule(ctx): any {
        return {
            postTransformNode: (node: ASTNode) => {
                for (const tag in this.defaultOptions) {
                    if ((tag === '*' || node.tag === tag) && node.attrs) {
                        const attributes = this.defaultOptions[tag];
                        if (typeof attributes === 'string') {
                            node.attrs.some((attr) =>
                                this.rewrite(attr, attributes, ctx)
                            );
                        } else if (Array.isArray(attributes)) {
                            attributes.forEach((item) =>
                                node.attrs.some((attr) =>
                                    this.rewrite(attr, item, ctx)
                                )
                            );
                        }
                    }
                }
            }
        };
    }

    /**
     * 处理特殊路径
     * @param attr
     * @param name
     * @param ctx
     * @returns {boolean}
     */
    private static rewrite(attr: Attr, name: string, ctx) {
        if (attr.name === name) {
            const value = attr.value;
            const realPath = value.replace(/\"/g, '');
            // only transform static URLs
            if (value.charAt(0) === '"' && value.charAt(value.length - 1) === '"') {
                const pathList = realPath[0] === '~' ? ['', 'src', 'no-use'] : ctx.path.split('/');
                attr.value = `"${path.join(pathList.slice(0, pathList.length - 1).join('/'), realPath.replace('~', ''))}"`.replace(/\\/g, '/');
                return true;
            }
        }
        return false;
    }
}
