/**
 * @Description: koa服务配置
 * @author Beon
 * @date 2021/3/12
*/
import * as Koa from 'koa';
import * as views from 'koa-views';
import BaseToolsRewrite from './middlewares/baseToolsRewrite';
import ModulesBuild from './middlewares/modulesBuild';
import RequireApi from './middlewares/requireApi';
import ServeStatic from './middlewares/serveStatic';
import ModuleRewrite from './middlewares/moduleRewrite';
import ServerPluginVue from './middlewares/serverPluginVue';
import ServerPluginStyle from './middlewares/serverPluginStyle';
import {Context, Next} from 'koa';
import {UtilsDec} from '../decorators/utilsDec';
import {Config} from '../types/config';
import ProxyGet from './middlewares/proxyGet';
import ProxyBase from './middlewares/proxyBase';

export interface PluginContext{
	app: Koa,
	root: string
}

export class Server {
	app: Koa
	@UtilsDec.userConfig
	userConfig: Config

	/**
	 * 构造函数
	 */
	constructor() {
	    this.app = new Koa();

	    // 错误拦截处理
	    this.app.use(Server.errorNew);

	    // 注册pug渲染
	    this.app.use(views(`${__dirname}/../../views`, {extension: 'pug'}) as any);

	    // 中间件注册
	    this.config();
	}

	/**
	 * 基础配置
	 * @returns {void}
	 */
	private config(): void {
	    const root: string = process.cwd();

	    // 构建上下文对象
	    const context: PluginContext = {
		    app: this.app,
	        root
	    };
	    this.app.use((ctx: Context, next: Next) => {
	        // 扩展ctx属性
	        Object.assign(ctx, context);
	        return next();
	    });

	    const resolvedPlugins = [
	        this.userConfig?.server?.http2 ? ProxyGet : ProxyBase,
		    ServerPluginStyle,
		    ModulesBuild,
		    ModuleRewrite,
		    RequireApi,
		    BaseToolsRewrite,
		    ServerPluginVue,
		    ServeStatic
	    ];
	    // 依次注册所有插件
	    resolvedPlugins.forEach((plugin) => plugin.install(context));
	}

	/**
	 * 错误拦截处理
	 * @param ctx
	 * @param next
	 * @returns {Promise<void>}
	 */
	private static async errorNew(
	    ctx: Koa.Context,
	    next: Next
	): Promise<void> {
	    try {
	        await next();
		    if (ctx.response.status !== 200) {
		    	const err = new Error('ctx.response.message');
	            await ctx.render('error', {
	        	    message: ctx.response.status,
	        	    error: err
	            });
		    }
	    } catch (err) {
		    ctx.response.status = err['statusCode'] || err['status'] || 500;
		    await ctx.render('error', {
	            message: err.message,
	            error: err
		    });
	    }
	}
}
