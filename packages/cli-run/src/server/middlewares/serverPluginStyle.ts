/**
 * @Description: 处理样式文件
 * @author Beon
 * @date 2021/4/9
*/
import * as path from 'path';
import {readFileSync} from 'fs';
import BaseStyleLoad from '../utils/baseStyleLoad';

export default class ServerPluginStyle{
    static install({ app, root }): void {
        app.use(async (ctx, next) => {
	        if (
	        	!(/.less|.sass|.scss|.css/).test(ctx.path) ||
		        (/.css/).test(ctx.path) && ctx.request.header.accept.includes('text/css')
	        ) {
		        return next();
	        }

	        const filePath = path.join(root, ctx.path);
	        let content = readFileSync(filePath, 'utf8');
	        const pathList = ctx.path.split('/');
	        pathList[pathList.length - 1].split('.')[1] !== 'css' && (content = await BaseStyleLoad.styleLoad[pathList[pathList.length - 1].split('.')[1]](content, ctx));
	        ctx.type = 'js';
	        ctx.body = BaseStyleLoad.createStyleDom(content);
        });
    }
}
