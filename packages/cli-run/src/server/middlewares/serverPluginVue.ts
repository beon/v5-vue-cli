/**
 * @Description: 对vue进行预处理
 * @author Beon
 * @date 2021/3/15
*/
import * as path from 'path';
import {readFileSync} from 'fs';
import TemplateDealStyle from '../templateDeal/templateDealStyle';
import TemplateDealMain from '../templateDeal/templateDealMain';
import TemplateDealTemplate from '../templateDeal/templateDealTemplate';
import DescriptorCache from '../utils/descriptorCache';

export default class ServerPluginVue{

    static install({ app, root }): void {
        app.use(async (ctx, next) => {
            if (!ctx.path.endsWith('.vue')) {
                return next();
            }

            // vue文件处理
            const filePath = path.join(root, ctx.path);
            const content = readFileSync(filePath, 'utf8');
            if (!ctx.query.type) {
                const newDescriptor = DescriptorCache.createDescriptor(content, filePath);
                await TemplateDealMain.transformMain(ctx, newDescriptor);
            }
            const descriptor = DescriptorCache.getDescriptor(filePath);
            if (ctx.query.type === 'template') {
                TemplateDealTemplate.transformTemplate(ctx, descriptor);
            }
            if (ctx.query.type === 'style') {
                await TemplateDealStyle.transformStyle(ctx, descriptor);
            }
        });
    }
}
