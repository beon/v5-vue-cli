/**
 * @Description: 利用es-module-lexer处理import依赖
 * @author Beon
 * @date 2021/3/15
*/
import {parse} from 'es-module-lexer';
import MagicString from 'magic-string';
import * as path from 'path';
import {UtilsDec} from '../../decorators/utilsDec';
import FileCheck from '../utils/fileCheck';

export default class ModuleRewrite{
    @UtilsDec.readBody
    private static readBody
    private static suffixList = ['', '.vue', '.js', '/index.vue', '/index.js']
        .reduce((all, item) => all.concat(...[
            {url: item, root: process.cwd()},
            {url: item, defaultUrl: '/', root: process.cwd()},
            {url: item, defaultUrl: '/', root: process.cwd() + '/src'},
        ]), [])
    private static modulesList = [{
        url: 'package.json',
        root: path.resolve(__dirname, '../../../node_modules/')
    }, {
        url: 'package.json',
        root: path.join(process.cwd(), '/node_modules/')
    }]
    private static spSuffixList = ['', '.vue', '.js', '/index.vue', '/index.js']
        .reduce((all, item) => all.concat(...[
            {url: item, defaultUrl: '/', root: process.cwd() + '/node_modules'}
        ]), [])

    /**
     * 注册文件import预处理
     * @param app
     */
    static install({ app }): void {
        app.use(async (ctx, next) => {
            await next();
            // 对类型是js的文件进行拦截
            if (ctx.body && ctx.response.is('js')) {
                // 读取文件中的内容
                const content = await this.readBody(ctx.body);
                // 先行处理require
                ctx.body = this.transformRequireToImport(content);
                // 重写import中无法识别的路径
                ctx.body = await this.rewriteImports(ctx.body, ctx);
            }
        });
    }

    /**
     * 利用MagicString和es-module-lexer重写import
     * @param source
     * @returns {string}
     */
    static async rewriteImports(source: string, ctx, suffixList?): Promise<string> {
        let imports = parse(source)[0];
        const magicString = new MagicString(source);
        if (imports.length) {
            for (let i = 0; i < imports.length; i++) {
                const { s, e, ss, se, n } = imports[i];
                let id = n;
                if (!id || id.includes('?vue&')) {continue;} // 排除已经处理过的vue路径
                id = id.replace('@/', '/src/');
                id = id.replace('~', '/src');
                const checkClass = new FileCheck(suffixList ? suffixList : this.suffixList, this.modulesList);
                const dealId = (await checkClass.resourcesPreFetch(id, ctx)).path;
                if (dealId) {
                    id = dealId;
                } else {
                    id = (await (new FileCheck(this.spSuffixList, []).resourcesPreFetch(id, ctx))).path;
                }
                if (id) {
                    if ((/\.png|\.jpg|\.jpge|\.svg/).test(id)) {
                        magicString.overwrite(s, e, id + '?import');
                    } else if ((/\.json/).test(id)) {
                        magicString.overwrite(s, e, id + '?importJson');
                    } else {
                        let ssSeSplit = magicString.slice(ss, se);
                        if (/\/node_modules/.test(id)) {
                            if (se > 0) {
                                const matchResult = ssSeSplit.match(/{.*}/g);
                                let getIdName = `auto_import_${(((1 + Math.random()) * new Date().getTime()) | 0).toString(16).replace(/-/g, '_')}`;
                                let getIdAll = `import ${getIdName} from "${id}?${n}"`;
                                let dealAutoImport = matchResult ? matchResult.reduce((all, item) => {
                                    ssSeSplit = ssSeSplit.replace(item, '');
                                    return all + item.substring(1, item.length - 1).split(',').reduce((eachString, name) => {
                                        name = name.replace(/\s/g, '');
                                        return eachString + `const ${name} = ${getIdName}.__esModule && ${getIdName}.default ? ${getIdName}.default.${name} : ${getIdName}.${name};\n`;
                                    }, '');
                                }, '') : '';
                                ssSeSplit = ssSeSplit.match(/import.*from/) ? ssSeSplit.match(/import.*from/)[0] : '';
                                ssSeSplit = ssSeSplit.substring(6, ssSeSplit.length - 4).replace(/[\s,]/g, '');
                                ssSeSplit.length && (dealAutoImport += `const ${ssSeSplit} = ${getIdName}.__esModule && ${getIdName}.default ? ${getIdName}.default : ${getIdName};\n`);
                                let newImports = `${getIdAll};\n${dealAutoImport}`;
                                magicString.overwrite(ss, se, newImports);
                            } else {
                                magicString.overwrite(s, e + 1, `"${id}").then(m => ({...(m.default.__esModule && m.default.default ? m.default.default : m.default),default: (m.default.__esModule && m.default.default ? m.default.default : m.default)}))`);
                            }
                        } else {
                            magicString.overwrite(s, e, se > 0 ? id : `"${id}"`);
                        }
                    }
                } else {
                    magicString.overwrite(ss, se > 0 ? se : e, `// 无法找到依赖${id}\nconsole.error("无法找到依赖${source.substring(s, e)}")`);
                }
            }
        }
        return magicString.toString();
    }

    /**
     * 转化require为import
     * @param code
     * @returns {string}
     */
    static transformRequireToImport(code: string): string {
        const imports: { [key: string]: string } = {};
        let strImports = '';

        code = code.replace(
            /require\(("(?:[^"\\]|\\.)+"|'(?:[^'\\]|\\.)+')\)/g,
            (_, name): any => {
                if (!(name in imports)) {
                    imports[name] = `__$_require_${name
                        .replace(/[^a-z0-9]/g, '_')
                        .replace(/_{2,}/g, '_')
                        .replace(/^_|_$/g, '')}__`;
                    strImports += 'import ' + imports[name] + ' from ' + name + '\n';
                }

                return imports[name];
            }
        );

        return strImports + code;
    }
}
