import * as fs from 'fs';
import * as path from 'path';
import {UtilsDec} from '../../decorators/utilsDec';
import {Config} from '../../types/config';

export default class BaseToolsRewrite{
	@UtilsDec.userConfig
	private static config: Config

	/**
	 * 注册文件import预处理
	 * @param app
	 */
	static install({ app }): void {
	    app.use(async (ctx, next) => {
	        if (ctx.path !== '/static/v5ToolsBaseFn.js') {
	            return next();
	        }

	        let code = fs.readFileSync(path.join(__dirname, '../../../static/v5ToolsBaseFn.js')).toString();
	        ctx.type = 'js';
	        ctx.body = this.addGlobalCode(code);
	    });
	}

	/**
	 * 添加配置的全局内容
	 * @param code
	 * @returns {string}
	 */
	private static addGlobalCode(code: string): string {
    	if (!this.config?.config?.global) {
		    return code;
	    }

    	const global = this.config.config.global;
    	Object.keys(global).forEach((item: string) => {
    		if (Object.prototype.toString.call(global[item]) === '[object Array]') {
			    code += `\nimport {${global[item][1]}} from "${global[item][0]}";\nwindow["${item}"] = ${global[item][1]}`;
		    } else {
			    code += `\nimport ${item} from "${global[item]}";\nwindow["${item}"] = ${item}`;
		    }
	    });
    	return code;
	}
}
