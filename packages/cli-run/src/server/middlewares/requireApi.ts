/**
 * @Description: 路径处理中间件
 * @author Beon
 * @date 2021/3/15
*/
import {PluginContext} from '../server';
import * as fs from 'fs';
import {join} from 'path';
import {UtilsDec} from '../../decorators/utilsDec';
import {Config, svgLoader} from '../../types/config';
import Tools, {RequireContextResult} from '../utils/tools';

type contextParam = [string, boolean, RegExp]

export default class RequireApi {
    @UtilsDec.userConfig
    private static userConfig: Config
    @UtilsDec.readBody
    private static readBody

    /**
     * 注册中间件
     * @param {PluginContext} app
     */
    static install({ app }: PluginContext): void {
        app.use(async (ctx, next) => {
            await next();
            // 对类型是js的文件进行拦截
            if (ctx.body && ctx.response.is('js')) {
                // 读取文件中的内容
                const content = await this.readBody(ctx.body);
                ctx.body = await this.setRequireParams(content, ctx);

                if (/new [vV]ue[\s]*\(/.test(ctx.body)) {
                    ctx.body = "import 'v5ToolsBaseFn'\n" + ctx.body;
                    this.userConfig?.config?.svgLoader && (ctx.body = ctx.body.replace(/new [vV]ue[\s]*\(/, `v5ToolCreateSvg('${this.dealSvgAdd(this.userConfig?.config?.svgLoader)}'); new Vue(`));
                }
            }
        });
    }

    /**
     * 处理添加svg
     * @param svgLoader
     * @returns {string}
     */
    private static dealSvgAdd(svgLoader: svgLoader): string {
        return Object.values(Tools.getJsonFiles(svgLoader.path, true, /\.svg$/)).reduce((all: string, item: RequireContextResult) => {
            const NAME = item.fileName.split('/')[item.fileName.split('/').length - 1].replace(/\.svg/, '');
            const REALNAME = svgLoader.symbolId.replace('[name]', NAME);
            all += `${this.reSetSvgData(fs.readFileSync(join(process.cwd(), item.url)).toString()
                .replace(/^\s*<svg/, `<symbol id="${REALNAME}"`)
                .replace(/<\/svg>\s*$/, '</symbol>'))}`;
            return all;
        }, '');
    }

    /**
     * svg Size
     * @param svg
     * @returns {string}
     */
    private static reSetSvgData(svg: string): string {
        const width = svg.match(/width=["']\d*[^"']/)[0].substring(7);
        const height = svg.match(/height=["']\d*[^"']/)[0].substring(8);
        return svg.replace(/width=["']\d*["']|height=["']\d*[^"']/g, '')
            .replace(/^\s*<symbol/, `<symbol viewBox="0 0 ${width} ${height}"`);
    }

    /**
     * 设置require的参数
     * @param content
     * @param ctx
     * @returns {Promise<string>}
     */
    private static async setRequireParams(content: string, ctx) {
        const regRquire = /require\.context[^)]*\)/g;
        const regParams = /\([^)]*/;
        // 处理匹配数量
        const matchList = content.match(regRquire);
        if (matchList) {
            for (const item of matchList) {
                let paramList = item.match(regParams)[0].substring(1).split(',');
                paramList = paramList.map((itemUrl) => itemUrl.replace(/(\\r)*(\\n)*\s*'*"*/g, ''));
                const importObj = await this.dealRequireContext(paramList, ctx);
                let paramString = '';
                for (const imName in importObj) {
                    content = `import ${imName} from '${importObj[imName].url}';\n` + content;
                    paramString += `"${importObj[imName].fileName}": {fileName: '${importObj[imName].fileName}', content: ${imName}},`;
                }
                content = content.replace(item, `new RequireContext({${paramString}})`);
            }
            content = "import {RequireContext} from 'vueComponentNormalizer';\n" + content;
        }
        return content;
    }

    /**
     * 处理require的主题内容
     * @param params
     * @param ctx
     * @returns {Promise<{}>}
     */
    private static async dealRequireContext(params: string[], ctx): Promise<{ [each: string]: RequireContextResult }> {
        const pathList = ctx.path.split('/');
        const realParams: contextParam = [
            join(process.cwd(), pathList.splice(0, pathList.length - 1).join('/'), params[0]),
            Boolean(params[1]),
            RegExp(params[2].substring(1, params[2].length - 1))
        ];
        return Tools.getJsonFiles(...realParams);
    }
}
