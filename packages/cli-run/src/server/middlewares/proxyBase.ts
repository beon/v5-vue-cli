import {PluginContext} from '../server';
import {UtilsDec} from '../../decorators/utilsDec';
import {Config} from '../../types/config';
import * as proxy from 'koa-server-http-proxy';

export default class ProxyBase{
	@UtilsDec.userConfig
	private static userConfig: Config

	/**
	 * 注册中间件
	 * @param {PluginContext} app
	 */
	static install({ app }: PluginContext): void {
	    // 代理服务
	    this.userConfig?.server?.proxy && Object.keys(this.userConfig?.server?.proxy).forEach((context) => {
		    const options = this.userConfig?.server?.proxy?.[context];
		    options.headers?.host !== undefined || options.headers
			    ? Object.assign(options.headers, {host: ''})
			    : Object.assign(options, {headers: {host: ''}});
		    app.use(proxy(context, options));
	    });
	}
}
