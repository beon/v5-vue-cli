/**
 * @Description: 利用esbuild打包三方依赖的esm
 * @author Beon
 * @date 2021/4/9
*/
import * as esbuild from 'esbuild';
import * as path from 'path';
import * as fs from 'fs';
import {parse} from 'es-module-lexer';
import MagicString from 'magic-string';
import Tools from '../utils/tools';

export default class ModulesBuild{
	private static fileUrl = '../../../'
	private static setDir = 'v5-modules'
	private static esbuildMetaPath = path.join(__dirname, ModulesBuild.fileUrl, '/node_modules/v5-modules/metaPath_build.json')
	private static modulesDir = [
	    path.join(__dirname, ModulesBuild.fileUrl),
	    process.cwd()
	]

	/**
	 * 注册文件import预处理
	 * @param app
	 */
	static install({ app }): void {
	    app.use(async (ctx, next) => {
        	const pathList = ctx.path.split('/');
		    if (pathList[1] !== 'node_modules' || !pathList[pathList.length - 1].includes('.js') || pathList[pathList.length - 1].includes('.json')) {
			    await next();
			    if (ctx.request.query.import !== undefined) {
				    ctx.type = 'js';
				    ctx.body = `export default "${ctx.path}"`;
			    }
			    if (ctx.request.query.importJson !== undefined) {
				    ctx.type = 'js';
				    ctx.body = this.createJsonExport(ctx.path);
			    }
			    pathList[pathList.length - 1].includes('.js') && (ctx.body = await this.dealFile(ctx.body));
		    } else {
			    ctx.type = 'js';
			    const moduleName = Object.keys(ctx.query)[0] ? Tools.dealSpPath(Object.keys(ctx.query)[0]) + '.js' : pathList[pathList.length - 1];
			    try {
				    ctx.body = this.dealAbPath(fs.readFileSync(path.join(process.cwd(), `/node_modules/${this.setDir}`, moduleName)).toString(), moduleName);
			    } catch (e) {
				    // 进入catch表示为node自带api
				    !fs.existsSync(path.join(__dirname, this.fileUrl, `/node_modules/${this.setDir}`, moduleName)) && await this.buildModule(ctx, moduleName);
				    ctx.body = fs.readFileSync(path.join(__dirname, this.fileUrl, `/node_modules/${this.setDir}`, moduleName)).toString();
			    }
		    }
	    });
	}

	private static dealAbPath(content, pathUrl): string {
	    const pathList = pathUrl.split('/').splice(0, pathUrl.split('/').length - 1);
	    const basePath = pathList.length ? pathList.join('/') : '';
	    let imports = parse(content)[0];
	    const magicString = new MagicString(content);
	    imports.forEach((item) => {
	        const { s, e, n } = item;
		    magicString.overwrite(s, e, `/node_modules/${path.join(basePath, n)}`);
	    });
	    return magicString.toString();
	}

	/**
	 * 利用esbuild进行代码esm转换
	 * @returns {Promise<TransformResult>}
	 * @param body
	 */
	private static async dealFile(body) {
	    if (!body) {return '';}
	    return (await esbuild.transform(body, {
	        format: 'esm',
	        sourcemap: true,
	        treeShaking: 'ignore-annotations'
	    })).code;
	}

	/**
	 * 利用esbuild进行三方依赖打包
	 * @param moduleName
	 * @returns {Promise<void>}
	 * @param ctx
	 */
	private static async buildModule(ctx, moduleName) {
	    for (const root of this.modulesDir) {
	    	if (fs.existsSync(path.join(root, ctx.path))) {
			    const result = await esbuild.build({
				    entryPoints: [path.join(root, ctx.path)],
				    bundle: true,
				    write: true,
				    format: 'esm',
				    sourcemap: true,
				    treeShaking: 'ignore-annotations',
				    metafile: true,
				    outfile: path.join(__dirname, this.fileUrl, `/node_modules/${this.setDir}`, moduleName)
			    });
	            this.createBuildMetafile(result, moduleName);
			    break;
		    }
	    }
	}

	/**
	 * 跟新创建三方依赖构建json
	 * @param result
	 * @param moduleName
	 */
	private static createBuildMetafile(result, moduleName: string) {
	    const baseFile = fs.existsSync(this.esbuildMetaPath)
	        ? fs.readFileSync(this.esbuildMetaPath).toString()
	        : '{}';
	    const resultBuild = JSON.parse(baseFile);
	    resultBuild[moduleName] = result.metafile;
	    fs.writeFileSync(
	        this.esbuildMetaPath,
	        JSON.stringify(resultBuild)
	    );
	}

	private static createJsonExport(pathUrl: string): string {
	    try {
	        const getJson = JSON.parse(fs.readFileSync(path.join(process.cwd(), pathUrl)).toString());
	        let allString = Object.keys(getJson).reduce((all, item) => all += `export const ${item} = ${JSON.stringify(getJson[item])};\n`, '');
	        allString += `export default {${Object.keys(getJson)}}`;
	        return allString;
	    } catch (e) {
	        return 'json转换出错';
	    }
	}
}
