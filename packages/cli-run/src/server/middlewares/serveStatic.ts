/**
 * @Description: 服务静态资源中间件
 * @author Beon
 * @date 2021/3/15
*/
import * as path from 'path';
import * as koaStatic from 'koa-static';
import {PluginContext} from '../server';

export default class ServeStatic{

    /**
     * 处理静态路径
     * @param app
     * @param root
     */
    static install({app, root}: PluginContext): void{
        // 以当前根目录作为服务静态目录
        app.use(koaStatic(path.join(__dirname, '../../../views')));
        app.use(koaStatic(path.join(__dirname, '../../../')));

        // 以项目目录作为根目录
        app.use(koaStatic(root));
        app.use(koaStatic(path.join(root, 'public')));
        app.use(koaStatic(path.join(root, 'src')));
    }
}
