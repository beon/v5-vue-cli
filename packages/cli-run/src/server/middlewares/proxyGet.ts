/**
 * @Description: http2加速的proxy代理
 * @author Beon
 * @date 2021/5/19
*/
import {PluginContext} from '../server';
import {UtilsDec} from '../../decorators/utilsDec';
import {Config} from '../../types/config';
import axios from 'axios';
import * as bodyParser from 'koa-bodyparser';
import * as qs from 'qs';

export default class ProxyGet{
	@UtilsDec.userConfig
	private static userConfig: Config
	private static proxyUrl: string[]

	/**
	 * 注册中间件
	 * @param {PluginContext} app
	 */
	static install({ app }: PluginContext): void {
	    this.userConfig?.server?.proxy && (this.proxyUrl = Object.keys(this.userConfig?.server?.proxy));

	    if (ProxyGet.proxyUrl?.length) {
		    app.use(bodyParser());
		    axios.defaults.baseURL = `http://localhost:${UtilsDec.ProxyPort}/`;
	    }

	    app.use(async (ctx, next) => {
	        if (!ProxyGet.proxyUrl || !ProxyGet.proxyUrl.length || !ProxyGet.checkProxyUrl(ctx.path)) {
		        return next();
	        }
		    const a = await this.axiosReq(ctx);
		    ctx.type = ctx.path.match(/\.[^?]*/)[0].substring(1);
		    ctx.body = a.data;
	    });
	}

	/**
	 * 校验判断
	 * @param path
	 * @returns {boolean}
	 */
	private static checkProxyUrl(path: string): boolean {
	    return this.proxyUrl.some((item) => path.includes(item));
	}

	/**
	 * 代理请求
	 * @param ctx
	 * @returns {AxiosPromise<any>}
	 */
	private static async axiosReq(ctx) {
	    const REQUEST = JSON.parse(JSON.stringify(ctx.request));
	    Object.keys(REQUEST.header).forEach((item) => {
	        if (item[0] === ':') {
	            delete REQUEST.header[item];
	        }
	    });
	    REQUEST.data = qs.stringify(ctx.request.body);
	    return axios(REQUEST);
	}
}
