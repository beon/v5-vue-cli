import {parse} from 'es-module-lexer';
import * as path from 'path';
import * as less from 'less';
import * as sass from 'sass';
import MagicString from 'magic-string';
import {UtilsDec} from "../../decorators/utilsDec";
import {Config} from "../../types/config";
import FileCheck, {SuffixObj} from "./fileCheck";

export enum importType {
	'SASS',
	'LESS'
}

interface SassRedis{
	[key: string]: {
		content?: string,
		result?: string,
		noDeal?: boolean
	}
}

export default class BaseStyleLoad{
	@UtilsDec.userConfig
	private static config: Config
	private static suffixList = ['']
	private static styleUrl = ['~', '~@', '@']
	private static sassRedis: SassRedis = {}

	// 样式处理
	static styleLoad = {
	    less: BaseStyleLoad.less,
	    sass: BaseStyleLoad.sass,
	    scss: BaseStyleLoad.sass
	}
	private static importDeal = {
		[importType.SASS]: BaseStyleLoad.dealSassImport,
		[importType.LESS]: BaseStyleLoad.dealLessImport
	}

	/**
	 * 处理style构建dom模板
	 * @param content
	 * @returns {string}
	 */
	static createStyleDom(content: string): string{
		let cssString = `const css = ${JSON.stringify(content)}
const style = document.createElement('style');
style.setAttribute('type', 'text/css');
style.innerHTML = css;
document.head.append(style)
`;
		const matchList = content.match(/:export\s{[^}]*}/);
		let exportCss = '';
		if(matchList && matchList.length) {
			exportCss = `export default ${JSON.stringify(matchList.reduce((all, item) => Object.assign(all, this.createStyleExport(item)), {}))}`;
		}
		cssString += exportCss || 'export default css';
	    return cssString;
	}

	private static createStyleExport(variables) {
		const dealStr = variables.replace(':export', '').replace(/;/g, '",').replace(/:\s*/g, '": "').replace(/\s/g, '').replace(/,/g, ', "')
		return JSON.parse(`{"${dealStr.substring(1, dealStr.length - 4)}}`)
	}

	/**
	 * 处理style中的import的路径
	 * @param content
	 * @param type
	 * @param pathList
	 * @returns {any}
	 */
	private static async dealImportLoad(content: string, type: importType, pathList: string[], ctx): Promise<string> {
	    if (content.includes('@import')) {
	        content = content.replace(/@import/g, '\/\*test\*\/import');
		    content = content.replace(/:export/g, '\/\*test\*\/scssep');
	        const imports = parse(content)[0];
	        let addLength = 0;
	        for(const item of imports) {
		        const { s, e, n } = item;
		        let rePath = n;
		        // 处理~路径
		        this.getSpUrl(n) && (rePath = await this.dealFilePath(n, ctx, this.getSpUrl(n)));
		        // 不同样式处理
		        rePath = this.importDeal[type](rePath, pathList);
		        (rePath !== n) && (content = (new MagicString(content)).overwrite(s + addLength, e + addLength, rePath).toString());
		        addLength += rePath.length - n.length;
	        }
	        content = content.replace(/\/\*test\*\/import/g, '@import');
		    content = content.replace(/\/\*test\*\/scssep/g, ':export');
	    }
	    const matchList = content.match(/\/\/v5-run-style-path\s*\n.*/);
	    if (matchList && matchList.length) {
		    matchList.forEach(item => {
			    content = content.replace(item, item.replace('~', '/node_modules/'))
		    })
	    }
	    return content;
	}

	/**
	 * 获取特殊路径
	 * @param SpPath
	 * @returns {string}
	 */
	private static getSpUrl(SpPath: string): string {
		return SpPath.match(/^[~@]*/) ? SpPath.match(/^[~@]*/)[0] : '';
	}

	/**
	 * 处理真实需求样式路径
	 * @param n
	 * @param ctx
	 * @param index
	 * @returns {Promise<string>}
	 */
	private static async dealFilePath(n: string, ctx, SpPath): Promise<string> {
		let id = n.replace(SpPath, '');
		const checkClass = new FileCheck(
			this.createSuffixList(['', `.${ctx.path.split('.')[ctx.path.split('.').length - 1]}`]),
			[]
		);
		id = (await checkClass.resourcesPreFetch(id, ctx)).path;
		return id.replace(/\//, '');
	}

	/**
	 * 获取suffix list
	 * @param list
	 * @returns {any}
	 */
	private static createSuffixList(list): SuffixObj[] {
		return list.reduce((all, item) => all.concat(...[
			{url: item, root: process.cwd()},
			{url: item, defaultUrl: '/', root: process.cwd()},
			{url: item, defaultUrl: '/', root: process.cwd() + '/src'},
			{url: item, defaultUrl: '/', root: process.cwd() + '/node_modules'}
		]), [])
	}

	/**
	 * less处理方式
	 * @param content
	 * @param ctx
	 * @returns {Promise<void>}
	 */
	private static async less(content: string, ctx): Promise<string> {
	    const pathList = ctx.path.split('/');
		content = await BaseStyleLoad.dealImportLoad(content, importType.LESS, pathList, ctx);
		content = `${BaseStyleLoad.addStyleLoaderImports(['less'])}${content}`;
		return await BaseStyleLoad.dealSassRedis(content, ctx, pathList, async (sendContent) => {
			return (await less.render(sendContent, {rootpath: pathList.slice(0, pathList.length - 1).join('/')})).css
		});
	}

	/**
	 * sass处理方式
	 * @param content
	 * @param ctx
	 * @returns {Promise<void>}
	 */
	private static async sass(content: string, ctx): Promise<string> {
	    const pathList = ctx.path.split('/');
		content = await BaseStyleLoad.dealImportLoad(content, importType.SASS, pathList, ctx);
		content = `${BaseStyleLoad.addStyleLoaderImports(['scss', 'sass'])}${content}`;
		return await BaseStyleLoad.dealSassRedis(content, ctx, pathList, async (sendContent) => {
			sendContent = sass.renderSync({
				data: sendContent,
				includePaths: [path.join(process.cwd(), pathList.slice(0, pathList.length - 1).join('/'))]
			}).css.toString();
			return (await less.render(sendContent, {rootpath: pathList.slice(0, pathList.length - 1).join('/')})).css
		});
	}

	/**
	 * 处理sass缓存
	 * @param content
	 * @param ctx
	 * @param pathList
	 * @returns {Promise<void>}
	 */
	private static async dealSassRedis(content: string, ctx, pathList: string[], dealFn) {
		if (BaseStyleLoad.sassRedis[ctx.path]) {
			if (content === BaseStyleLoad.sassRedis[ctx.path].content) {
				content = BaseStyleLoad.sassRedis[ctx.path].result;
			} else {
				content = await dealFn(content);
			}
		} else {
			content = content.replace(/@import/g, '\/\*test\*\/import');
			content = content.replace(/:export/g, '\/\*test\*\/scssep');
			const imports = parse(content)[0];
			content = content.replace(/\/\*test\*\/import/g, '@import');
			content = content.replace(/\/\*test\*\/scssep/g, ':export');
			if (!imports.some(({n}) => !n.includes("node_modules/"))) {
				BaseStyleLoad.sassRedis[ctx.path] = {
					content
				}
			} else {
				BaseStyleLoad.sassRedis[ctx.path] = {
					noDeal: true
				}
			}
			content = await dealFn(content);
			BaseStyleLoad.sassRedis[ctx.path] && (BaseStyleLoad.sassRedis[ctx.path].result = content);
		}
		return content;
	}

	/**
	 * 处理less的import路径问题（scss也是一样）
	 * @param rePath
	 * @param pathList
	 * @returns {string}
	 */
	private static dealLessImport(rePath: string, pathList: string[]): string {
		if (rePath.substring(0,4) === '/src') {
			return rePath.substring(1);
		} else if(rePath.substring(0,4) === 'src/') {
			return rePath;
		} else {
			return path.join(pathList.slice(1, pathList.length - 1).join('/'), rePath).replace(/\\/g, '/');
		}
	}

	/**
	 * 处理less的import路径问题（scss也是一样）
	 * @param rePath
	 * @param pathList
	 * @returns {string}
	 */
	private static dealSassImport(rePath: string, pathList: string[]): string {
		return rePath;
	}

	/**
	 * 添加默认的基础公共引入
	 * @param type
	 * @returns {string}
	 */
	private static addStyleLoaderImports(type: string[]): string {
		return type.reduce((all, item) => {
			const ImportList = this.config?.config?.styleLoader?.[item];
			all += ImportList?.length > 0
				? ImportList.reduce((allImport, eachImport) => {
					allImport += `@import "${eachImport}";\n`;
					return allImport;
				}, '')
				: '';
			return all;
		}, '');
	}
}
