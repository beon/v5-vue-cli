/**
 * @Description: 缓存vue结构信息保证每个vue文件的隔离
 * @author Beon
 * @date 2021/4/9
*/
import {parse, SFCDescriptor} from '@vue/component-compiler-utils';
import * as vueTemplateCompiler from 'vue-template-compiler';
import * as hash from 'hash-sum';

export default class DescriptorCache{
	private static cache = new Map<string, SFCDescriptor>()

	/**
	 * 缓存每个vue的解构信息
	 * @param source
	 * @param filename
	 * @returns {SFCDescriptor}
	 */
	static createDescriptor(
	    source: string,
	    filename: string
	): SFCDescriptor{
	    const descriptor = parse({
		    source,
		    compiler: vueTemplateCompiler as any,
		    filename,
	        needMap: true,
	    });
	    descriptor.id = hash(filename + source);

	    this.cache.set(filename, descriptor);
	    return descriptor;
	}

	/**
	 * 获取解构信息
	 * @param filename
	 * @returns {SFCDescriptor}
	 */
	static getDescriptor(filename: string): SFCDescriptor {
	    return this.cache.get(filename);
	}
}
