import * as path from 'path';
import SpicalModuleUrl from './spicalModuleUrl';
import {readFileSync, statSync} from 'fs';

enum fileType{
	'vue',
	'js',
	'css'
}

interface checkResult<T>{
	type: fileType,
	done: T,
	path?: string
}

export interface SuffixObj{
	url: string,
	root: string,
	defaultUrl?: string
}

export interface modulesObj{
	url: string,
	root: string
}

export default class FileCheck {
	private readonly suffixList: SuffixObj[]
	private readonly modulesList: modulesObj[]
	private resourcesPriorityList = [
		FileCheck.checkDealModules,
		this.checkDefaultUrl.bind(this),
		this.modulesCheck.bind(this)
	]

	constructor(suffixList, modulesList) {
		this.suffixList = suffixList;
		this.modulesList = modulesList;
	}

	/**
	 * 主入口判断方法
	 * @param id
	 * @param ctx
	 * @returns {Promise<any | {type: fileType, done: undefined}>}
	 */
	async resourcesPreFetch(id: string, ctx): Promise<checkResult<boolean>> {
	    for (let i = 0; i < this.resourcesPriorityList.length ; i++) {
	        const result = await this.resourcesPriorityList[i](id, ctx);
	        if (result.done && result.path !== ctx.path) {
	            return result;
	        }
	    }
	    return {
	        type: fileType.js,
	        done: undefined
	    };
	}

	/**
	 * 判断node_modules的依赖
	 * @param id
	 * @param ctx
	 * @returns {Promise<{type: fileType, done: undefined} | checkResult<boolean>>}
	 */
	private async modulesCheck(id: string, ctx): Promise<checkResult<boolean>> {
	    if (this.modulesList && this.modulesList.length) {
		    for (let i = 0 ; i < this.modulesList.length ; i++) {
			    const item = this.modulesList[i];
			    const result = await FileCheck.checkFileExist<string>(path.join(id, item.url), item.root, true);
			    if (result.done) {
				    const moduleResult = await FileCheck.checkFileExist<boolean>(`${id}/${JSON.parse(result.done).main}`, item.root);
				    if (moduleResult.done) {
					    return moduleResult;
				    } else {
					    const moduleResultJS = await FileCheck.checkFileExist<boolean>(`${id}/${JSON.parse(result.done).main}.js`, item.root);
					    if (moduleResultJS.done) {
						    return moduleResultJS;
					    }
				    }
			    }
		    }
	    }
	    return {
	        type: fileType.js,
	        done: undefined
	    };
	}

	/**
	 * 判断suffixList的基础流程
	 * @param id
	 * @param ctx
	 * @returns {Promise<{type: fileType, done: undefined} | checkResult<boolean>>}
	 */
	private async checkDefaultUrl(id: string, ctx): Promise<checkResult<boolean>> {
		if (this.suffixList && this.suffixList.length) {
			const pathList = ctx.path.replace(ctx.request.header.origin, '').replace(/\\/g, '/').split('/');
			const defaultUrl = path.join(pathList.slice(0, pathList.length - 1).join('/'), id);
			for (let i = 0 ; i < this.suffixList.length ; i++) {
				const item = this.suffixList[i];
				const result = await FileCheck.checkFileExist<boolean>(
					(item.defaultUrl ? path.join(item.defaultUrl, id) : defaultUrl) +
					(item.url[0] === '/' ? path.join(item.url) : item.url)
					, item.root);
				if (result.done) {
					return result;
				}
			}
		}
	    return {
	        type: fileType.js,
	        done: undefined
	    };
	}

	/**
	 * 处理三方已处理依赖
	 * @param id
	 * @param ctx
	 * @returns {Promise<{path: any, type: fileType, done: boolean}>}
	 */
	private static async checkDealModules(id: string, ctx): Promise<checkResult<boolean>> {
	    return {
	        type: fileType.js,
	        path: SpicalModuleUrl[id] ? SpicalModuleUrl[id].replace(path.resolve(__dirname, '../../../'), '').replace(/\\/g, '\/') : undefined,
	        done: SpicalModuleUrl[id] ? true : undefined
	    };
	}

	/**
	 * 校验文件存在性
	 * @param url
	 * @param root
	 * @param fileContent
	 * @returns {Promise<{path: string, type: any, done: any}>}
	 */
	private static async checkFileExist<T>(url: string, root: string, fileContent?: boolean): Promise<checkResult<T>> {
	    let done;
	    try {
	        done = fileContent
	            ? readFileSync(path.join(root, url), 'utf8')
	            : statSync(path.join(root, url)).isFile();
	    } catch (e) {
	        // console.log(e);
	    }
	    return {
	        type: fileType[url.split('.')[url.split('.').length - 1]],
	        done,
	        path: path.join(root, url)
	            .replace(path.resolve(__dirname, '../../../'), '')
	            .replace(process.cwd(), '')
	            .replace(/\\/g, '\/')
	    };
	}
}
