import * as fs from 'fs';
import {join} from 'path';

export interface RequireContextResult {
	url: string,
	fileName: string
}

export default class Tools {

    /**
	 * 通过参数获取文件列表
	 * @param jsonPath
	 * @param deep
	 * @param rules
	 * @returns {{}}
	 */
    static getJsonFiles(jsonPath: string, deep: boolean, rules: RegExp): {[each: string]: RequireContextResult } {
        let jsonFiles = {};
        function findJsonFile(pathList = []) {
            try {
                let files = fs.readdirSync(join(jsonPath, ...pathList));
                files.forEach((item) => {
                    const fPath = join(jsonPath, ...pathList, item);
                    const stat = fs.statSync(fPath);
                    deep && stat.isDirectory() === true && findJsonFile(pathList.concat(item));
                    stat.isFile() === true && rules.test(item) && (jsonFiles[fPath.replace(/[:\-\\\/\.]/g, '')] = {
                        url: fPath.replace(process.cwd(), '').replace(/\\/g, '/'),
                        fileName: `./${pathList.length > 0 ? pathList.join('/') + '/' : ''}${item}`
                    });
                });
            } catch (e) {
                // console.log(e);
            }
        }
        findJsonFile();
        return jsonFiles;
    }
    static dealSpPath(pathUrl:string): string {
        return pathUrl.replace(/[\/\.]/g, '_');
    }
}
