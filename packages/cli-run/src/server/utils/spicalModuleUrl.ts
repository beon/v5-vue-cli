/**
 * @Description: 内部文件导出支持
 * @author Beon
 * @date 2021/4/9
*/
import * as path from 'path';

const SpicalModuleUrl = {
    v5ToolsBaseFn: path.join(__dirname, '../../../static/v5ToolsBaseFn.js'),
    vueComponentNormalizer: path.join(__dirname, '../../../static/componentNormalizer.js')
};

export default SpicalModuleUrl;
