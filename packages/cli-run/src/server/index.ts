/**
 * @Description: 服务启动入口
 * @author Beon
 * @date 2021/3/11
*/
import {Server} from './server';
import * as Koa from 'koa';
import '@vue/component-compiler-utils';
import * as http from 'http';
import * as http2 from 'http2';
import * as fs from 'fs';
import * as path from 'path';
import {UtilsDec} from '../decorators/utilsDec';
import {Config} from '../types/config';

// 拓展descriptor;
declare module '@vue/component-compiler-utils' {
	interface SFCDescriptor {
		id: string
	}
}

export default class CreateService {
	@UtilsDec.userConfig
	userConfig: Config
	port: number | string | boolean
	app: Koa
	defaultPort = 3000
	retryTimes = 10
	checkTimes = 0

	/**
	 * 构建函数
	 */
	constructor() {
	    console.time('启动耗时：');
	    this.app = (new Server()).app;
	    let port: number | string | boolean;
	    port = this.normalizePort(process.env.PORT || String(this.defaultPort));
	    this.portIsOccupied(port);
	}

	/**
	 * 处理端口号
	 * @param {String} val 传入端口号
	 * @returns {string | boolean | number} 返回端口信息
	 */
	normalizePort(val: string): number {
	    const port = parseInt(val, 10);

	    if (isNaN(port)) {
	        // named pipe
	        return this.defaultPort;
	    }

	    if (port >= 0) {
	        // port number
	        this.port = port;
	        return port;
	    }
	    return this.defaultPort;
	}

	portIsOccupied (port: number): void {
	    if (this.retryTimes < this.checkTimes) {throw Error('端口被占用，请更改端口');}
	    this.checkTimes++;
	    // 创建服务并监听该端口
	    const server = http.createServer().listen(port);

	    server.on('listening', () => { // 执行这块代码说明端口未被占用
	        server.close(); // 关闭服务

		    if (this.userConfig?.server?.http2) {
			    http2.createSecureServer({
				    cert: fs.readFileSync(path.join(__dirname, '../ssl/cert.pem')),
				    key: fs.readFileSync(path.join(__dirname, '../ssl/key.pem'))
			    }, this.app.callback()).listen(port);
		    } else {
		    	http.createServer(this.app.callback()).listen(port);
		    }
		    // http.createServer(this.app.callback()).listen(port);
		    console.timeEnd('启动耗时：');
		    console.log(`服务开启成功:http${this.userConfig?.server?.http2 ? 's' : ''}://localhost:${port}`);
	    });

	    server.on('error', (err) => {
	        // @ts-ignore
		    if (err.code === 'EADDRINUSE') { // 端口已经被使用
		    	console.log(`端口${port}已被占用，尝试为您切换端口`);
	            this.portIsOccupied(++port);
	        }
	    });
	}
}
