import {CreateProxyService} from './plugins/proxy';
import CreateService from './server';
import PluginsIndex from './plugins';

(async () => {
    await PluginsIndex.createOptimizer();

    const app1 = new CreateProxyService(() => new CreateService());
})();
