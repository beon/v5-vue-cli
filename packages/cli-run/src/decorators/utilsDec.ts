/**
 * @author WYX
 * @date 2020/5/15
 * @Description: 工具装饰器类
*/
import * as path from 'path';
import * as fs from 'fs';
import * as Readable from 'stream';
import {Config} from '../types/config';

export class UtilsDec {
    private static CONFIGCACHE: Config
    static ProxyPort = 4000

    /**
     * 返回实例化当前对象
     * @param {*} Target 传递的参数类
     * @param {String} propertyKey 传递的参数名
     * @returns {void}
     */
    static newSelf(Target: any, propertyKey: string): void {
        Target[propertyKey] = new Target();
    }

    /**
     * 获取
     * @param Target
     * @param propertyKey
     */
    static userConfig(Target: any, propertyKey: string): void{
        if (!UtilsDec.CONFIGCACHE){
            let fileConfig;
            const configPath = path.resolve(process.cwd(), 'v5-run.config.js');
            if (fs.existsSync(configPath)) {
                try {
                    fileConfig = require(configPath);
                    if (typeof fileConfig === 'function') {
                        fileConfig = fileConfig();
                    }

                    if (!fileConfig || typeof fileConfig !== 'object') {
                        fileConfig = null;
                    }
                } catch (e) {
                    // console.log(e);
                }
            }
            UtilsDec.CONFIGCACHE = fileConfig;
        }

        Target[propertyKey] = UtilsDec.CONFIGCACHE;
    }

    /**
     * 获取文件内容
     * @returns {Promise<string>}
     * @param target
     * @param propertyKey
     */
    static readBody(target: any, propertyKey: string): void {
        target[propertyKey] = (stream: Readable | string): Promise<string> | string => {
            if (stream instanceof Readable) { //
                return new Promise((resolve) => {
                    let res = '';
                    stream
                        .on('data', (chunk) => res += chunk)
                        .on('end', () => resolve(res));
                });
            } else {
                return stream.toString();
            }
        };
    }
}
