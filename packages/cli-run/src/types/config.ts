// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Config{
	interface proxy{
		[url: string]: proxyParams
	}

	interface proxyParams {
		target: string,
		host?: string,
		headers?: {[key: string]: string}
		changOrigin?: boolean
	}

	interface server{
		proxy?: proxy
		http2?: boolean
		enter?: string[]
	}

	interface svgLoader{
		path: string,
		symbolId: string
	}

	interface styleLoader {
		[loadStyle: string]: string[]
	}

	interface global {
		[url: string]: string | string[]
	}

	interface config {
		svgLoader?: svgLoader
		styleLoader?: styleLoader
		global?: global
	}

	interface Config{
		config?: config
		server?: server
	}
}
export = Config
