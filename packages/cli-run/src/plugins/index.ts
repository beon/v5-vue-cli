import Scan from './optimizer/scan';
import EsbuildDepPlugin from './optimizer/esbuildDepPlugin';
import * as esbuild from 'esbuild';
import * as fs from 'fs';
import {parse, ImportSpecifier} from 'es-module-lexer';
import * as path from 'path';
import PluginsTools from "./tools/pluginsTools";

export type ExportsData = [ImportSpecifier[], string[]] & {
    // es-module-lexer has a facade detection but isn't always accurate for our
    // use case when the module has default export
    hasReExports?: true
}

export default class PluginsIndex{
    static async createOptimizer(): Promise<void> {
        const {deps} = await Scan.scanImports();

        const flatIdDeps: Record<string, string> = {};
        const idToExports: Record<string, ExportsData> = {};
        const flatIdToExports: Record<string, ExportsData> = {};

        for (const id in deps) {
            const flatId = PluginsTools.dealSpPath(id);
            flatIdDeps[flatId] = deps[id];
            const entryContent = fs.readFileSync(path.join(process.cwd(), deps[id]), 'utf-8');
            const exportsData = parse(entryContent) as ExportsData;
            for (const { ss, se } of exportsData[0]) {
                const exp = entryContent.slice(ss, se);
                if (/export\s+\*\s+from/.test(exp)) {
                    exportsData.hasReExports = true;
                }
            }
            idToExports[id] = exportsData;
            flatIdToExports[flatId] = exportsData;
        }

        await esbuild.build({
            entryPoints: Object.keys(flatIdDeps),
            bundle: true,
            keepNames: true,
            format: 'esm',
            logLevel: 'error',
            splitting: true,
            sourcemap: true,
            outdir: path.join(process.cwd(), './node_modules/v5-modules'),
            platform: 'node',
            treeShaking: 'ignore-annotations',
            metafile: true,
            plugins: [await EsbuildDepPlugin.createPlugin(flatIdDeps, flatIdToExports)]
        });
    }
}
