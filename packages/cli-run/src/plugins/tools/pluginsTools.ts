import * as fs from 'fs';

export const JS_TYPES_RE = /\.(?:j|t)sx?$|\.mjs$/;
export const htmlTypesRE = /\.(html|vue|svelte)$/;

export const KNOWN_ASSET_TYPES = [
    // images
    'png',
    'jpe?g',
    'gif',
    'svg',
    'ico',
    'webp',
    'avif',

    // media
    'mp4',
    'webm',
    'ogg',
    'mp3',
    'wav',
    'flac',
    'aac',

    // fonts
    'woff2?',
    'eot',
    'ttf',
    'otf',

    // other
    'wasm'
];

export const externalRE = /^(https?:)?\/\//;
export const isExternalUrl = (url: string) => externalRE.test(url);

export const OPTIMIZABLE_ENTRY_RE = /\.(?:m?js|ts)$/;


export default class PluginsTools{
    static deleteFolder(path: string): void {
        let files = [];
        if (fs.existsSync(path)) {
            files = fs.readdirSync(path);
            files.forEach(function(file){
                let curPath = path + '/' + file;
                if (fs.statSync(curPath).isDirectory()) {
                    arguments.callee(curPath);
                } else {
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    }
    static dealSpPath(pathUrl:string): string {
        return pathUrl.replace(/[\/\.]/g, '_');
    }
}
