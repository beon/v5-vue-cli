/**
 * @Description: 利用esbuild进行项目依赖分析
 * @author Beon
 * @date 2021/5/20
*/
import * as path from 'path';
import * as esbuild from 'esbuild';
import Tools from '../../server/utils/tools';
import * as fs from 'fs';
import FileCheck from '../../server/utils/fileCheck';
import DescriptorCache from '../../server/utils/descriptorCache';
import TemplateDealMain from '../../server/templateDeal/templateDealMain';
import TemplateDealTemplate from '../../server/templateDeal/templateDealTemplate';
import PluginsTools, {htmlTypesRE, JS_TYPES_RE, OPTIMIZABLE_ENTRY_RE} from '../tools/pluginsTools';
import {UtilsDec} from '../../decorators/utilsDec';
import {Config} from '../../types/config';

const srcRE = /\bsrc\s*=\s*(?:"([^"]+)"|'([^']+)'|([^\s'">]+))/im;
const scriptModuleRE = /(<script\b[^>]*type\s*=\s*(?:"module"|'module')[^>]*>)(.*?)<\/script>/gims;
const scriptRE = /(<script\b[^>]*>)(.*?)<\/script>/gims;

export default class Scan {
	@UtilsDec.userConfig
	private static userConfig: Config
	private static suffixList = ['', '.vue', '.js', '/index.vue', '/index.js']
	    .reduce((all, item) => all.concat(...[
	        {url: item, root: process.cwd()},
	        {url: item, defaultUrl: '/', root: process.cwd()},
	        {url: item, defaultUrl: '/', root: process.cwd() + '/src'},
	    ]), [])
	private static modulesList = [{
	    url: 'package.json',
	    root: path.join(process.cwd(), '/node_modules/')
	}]
	private static spSuffixList = ['', '.vue', '.js', '/index.vue', '/index.js']
	    .reduce((all, item) => all.concat(...[
	        {url: item, defaultUrl: '/', root: process.cwd() + '/node_modules'}
	    ]), [])
	private static fileUrl = 'node_modules'
	private static scanList = ['./', './public']
	private static scanPath = 'temp';

	static async scanImports(): Promise<{
		deps: Record<string, string>
	}> {
	    let entries: string[] = this.getEntries();
	    const tempDir = path.join(process.cwd(), this.fileUrl, this.scanPath);
	    const deps: Record<string, string> = {};
	    const plugin = this.esbuildScanPlugin(deps, entries);
	    await Promise.all(
	        entries.map((entry) =>
	            esbuild.build({
	                entryPoints: [entry],
	                bundle: true,
	                format: 'esm',
	                logLevel: 'error',
	                outdir: tempDir,
		            platform: 'node',
	                plugins: [plugin]
	            })
	        )
	    );
	    PluginsTools.deleteFolder(tempDir);

	    return {
	        deps
	    };
	}

	private static getEntries(): string[] {
	    let entries = this.scanList.concat(...this.userConfig?.server?.enter).reduce((all, item) => all.concat(Object.values(Tools.getJsonFiles(path.join(process.cwd(), item), false, htmlTypesRE)).map((each) => path.join(process.cwd(), item, each.fileName))), []);

	    if (!entries.length) {
	        console.log('没有发现入口html文件，请检查server.enter配置是否正常');
	        process.exit(1);
	    } else {
	        console.log(`检测到以下入口html文件:\n  ${entries.join('\n  ')}`);
	    }

	    return entries;
	}

	private static esbuildScanPlugin(
	    depImports: Record<string, string>,
	    entries: string[]
	) {
	    const seen = new Map<string, string | undefined>();

	    const resolve = async (id: string, importer?: string) => {
	        const key = id + (importer && path.dirname(importer));
	        if (seen.has(key)) {
	            return seen.get(key);
	        }
	        const res = path.join(importer, id);
	        seen.set(key, res);
	        return res;
	    };

	    const externalUnlessEntry = ({ path }: { path: string }) => ({
	        path,
	        external: !entries.includes(path)
	    });

	    function shouldExternalizeDep(resolvedId: string, rawId: string) {
	        // not a valid file path
	        if (!path.isAbsolute(resolvedId)) {
	            return true;
	        }
	        // virtual id
	        if (resolvedId === rawId || resolvedId.includes('\0')) {
	            return true;
	        }
	        // resovled is not a scannable type
	        if (!JS_TYPES_RE.test(resolvedId) && !htmlTypesRE.test(resolvedId)) {
	            return true;
	        }
	    }

	    return {
	        name: 'dep-scan',
	        setup(build) {
	            // bare imports: record and externalize ----------------------------------
	            build.onResolve(
	                {
	                    // avoid matching windows volume
	                    filter: /(\.[js|vue])|[^.]/
	                },
	                async ({ path: id, importer }) => {
		                if (!id || id.includes('?vue&')) {return {path: id};}
		                if (depImports[id]) {
	                        return externalUnlessEntry({ path: id });
	                    }
		                id = id.replace('@/', '/src/');
		                id = id.replace('~', '/src');
		                const checkClass = new FileCheck(Scan.suffixList, Scan.modulesList);
	                    let resolved = (await checkClass.resourcesPreFetch(id, {path: importer, request: { header: {origin: process.cwd()} }})).path;
		                if (!resolved) {
			                resolved = (await (new FileCheck(Scan.spSuffixList, []).resourcesPreFetch(id, {path: importer, request: { header: {origin: process.cwd()} }}))).path;
		                }
	                    if (resolved) {
	                        if (shouldExternalizeDep(resolved, id)) {
	                            return externalUnlessEntry({ path: path.join(id === 'vueComponentNormalizer' ? path.join(__dirname, '../../../') : process.cwd(), resolved) });
	                        }
	                        if (resolved.includes('node_modules')) {
	                            // dep or fordce included, externalize and stop crawling
	                            if (OPTIMIZABLE_ENTRY_RE.test(resolved)) {
	                                depImports[id] = resolved;
	                            }
	                            return externalUnlessEntry({ path: path.join(process.cwd(), resolved) });
	                        } else {
	                            // linked package, keep crawling
	                            return {
	                                path: path.join(id === 'vueComponentNormalizer' ? path.join(__dirname, '../../../') : process.cwd(), resolved)
	                            };
	                        }
	                    }
	                }
	            );

		        // html types: extract script contents -----------------------------------
		        build.onResolve({ filter: htmlTypesRE }, async ({ path, importer }) => ({
				        path: await resolve(path, importer),
				        namespace: 'html'
			        }));

		        // html types: extract script contents -----------------------------------
		        build.onLoad({ filter: /\.vue/ }, async ({ path: id, importer }) => {
			        const ctx = {body: null, path: id};
			        if (!/.*\?.*/.test(id)) {
				        const content = fs.readFileSync(id, 'utf-8');
				        const newDescriptor = DescriptorCache.createDescriptor(content, id);
				        await TemplateDealMain.transformMain(ctx, newDescriptor);
			        }
			        const descriptor = DescriptorCache.getDescriptor(id.replace(/\?.*/, ''));
			        if (/.*type=template.*/.test(id)) {
				        TemplateDealTemplate.transformTemplate(ctx, descriptor);
			        }
			        if (/.*type=style.*/.test(id)) {
				        ctx.body = '';
			        }
			        return {
			        	contents: ctx.body,
				        loader: 'js'
			        };
		        });

		        build.onLoad({ filter: htmlTypesRE, namespace: 'html' }, ({path: id}): any => {
			        const raw = fs.readFileSync(id, 'utf-8');
			        const regex = id.endsWith('.html') ? scriptModuleRE : scriptRE;
			        regex.lastIndex = 0;
			        let js = '';
			        let match;
			        while ((match = regex.exec(raw))) {
				        const [, openTag, content] = match;
				        const srcMatch = openTag.match(srcRE);
				        if (srcMatch) {
					        let src = srcMatch[1] || srcMatch[2] || srcMatch[3];
					        if (src[0] === '/') {
						        src = path.join(process.cwd(), src);
					        }
					        js += `import ${JSON.stringify(src)}\n`;
				        } else if (content.trim()) {
					        js += content + '\n';
				        }
			        }

			        // <script setup> may contain TLA which is not true TLA but esbuild
			        // will error on it, so replace it with another operator.
			        if (js.includes('await')) {
				        js = js.replace(/\bawait\s/g, 'void ');
			        }

			        if (!js.includes('export default')) {
				        js += 'export default {}';
			        }

			        return {
				        loader: 'js',
				        contents: js,
				        resolveDir: path.join(id, '../')
			        };
		        });
	        }
	    };
	}
}
