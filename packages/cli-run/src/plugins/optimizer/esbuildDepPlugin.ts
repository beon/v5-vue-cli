import * as path from 'path';
import {Plugin} from 'esbuild';
import {ExportsData} from '../index';
import FileCheck from '../../server/utils/fileCheck';
import PluginsTools, {isExternalUrl, KNOWN_ASSET_TYPES} from '../tools/pluginsTools';

const externalTypes = [
    'css',
    // supported pre-processor types
    'less',
    'sass',
    'scss',
    'styl',
    'stylus',
    'postcss',
    // known SFC types
    'vue',
    'svelte',
    // JSX/TSX may be configured to be compiled differently from how esbuild
    // handles it by default, so exclude them as well
    'jsx',
    'tsx',
    ...KNOWN_ASSET_TYPES
];

export default class EsbuildDepPlugin{
    private static modulesListFn(root?) {
        return [{
            url: 'package.json',
            root: path.join(root ? root : process.cwd(), '/node_modules/')
        }];
    }
    private static spSuffixListFn(root?) {
        return ['', '.vue', '.js', '/index.vue', '/index.js']
            .reduce((all, item) => all.concat(...[
                {url: item, defaultUrl: '/', root: path.join(root ? root : process.cwd(), '/node_modules')}
            ]), []);
    }

    static async createPlugin(
        qualified: Record<string, string>,
        exportsData: Record<string, ExportsData>
    ): Promise<Plugin> {
        async function resolve(id, importer, root?):Promise<string> {
            id = id.replace('@/', '/src/');
            id = id.replace('~', '/src');
            const checkClass = new FileCheck([], EsbuildDepPlugin.modulesListFn(root));
            let resolved = (await checkClass.resourcesPreFetch(id, {path: importer, request: { header: {origin: process.cwd()} }})).path;
            if (!resolved) {
                resolved = (await (new FileCheck(EsbuildDepPlugin.spSuffixListFn(root), []).resourcesPreFetch(id, {path: importer, request: { header: {origin: process.cwd()} }}))).path;
            }
            if (!resolved && !root) {
                resolved = await resolve(id, importer, importer.split('\\').splice(0, importer.split('\\').lastIndexOf('node_modules') + 2).join('\\'));
            }
            return resolved;
        }

        function resolveEntry(id: string, isEntry: boolean) {
            const flatId = PluginsTools.dealSpPath(id);
            if (flatId in qualified) {
                return isEntry
                    ? {
                        path: flatId,
                        namespace: 'dep'
                    }
                    : {
                        path: path.join(process.cwd(), qualified[flatId])
                    };
            }
        }

        return {
            name: 'dep-pre-bundle',
            setup(build) {
                // externalize assets and commonly known non-js file types
                build.onResolve(
                    {
                        filter: new RegExp('\\.(' + externalTypes.join('|') + ')(\\?.*)?$')
                    },
                    async ({ path: id, importer }) => {
                        const resolved = await resolve(id, importer);
                        if (resolved) {
                            return {
                                path: resolved,
                                external: true
                            };
                        }
                    }
                );

                build.onResolve(
                    { filter: /^[\w@][^:]/ },
                    async ({ path: id, importer }) => {
                        const isEntry = !importer;
                        // ensure esbuild uses our resolved entires
                        let entry;
                        // if this is an entry, return entry namespace resolve result
                        if ((entry = resolveEntry(id, isEntry))) {
                            return entry;
                        }

                        let resolvePath = await resolve(id, importer);
                        if (!resolvePath) {
                            return {
                                path: id,
                                external: true
                            };
                        }
                        const resolved = path.join(process.cwd(), resolvePath);
                        if (resolved) {
                            if (isExternalUrl(resolved)) {
                                return {
                                    path: resolved,
                                    external: true
                                };
                            }
                            return {
                                path: path.resolve(resolved)
                            };
                        }
                    }
                );

                // For entry files, we'll read it ourselves and construct a proxy module
                // to retain the entry's raw id instead of file path so that esbuild
                // outputs desired output file structure.
                // It is necessary to do the re-exporting to separate the virtual proxy
                // module from the actual module since the actual module may get
                // referenced via relative imports - if we don't separate the proxy and
                // the actual module, esbuild will create duplicated copies of the same
                // module!
                const root = process.cwd();
                build.onLoad({ filter: /.*/, namespace: 'dep' }, ({ path: id }): any => {
                    let relativePath = qualified[id];

                    if (!relativePath.startsWith('.')) {
                        relativePath = `.${relativePath}`;
                    }

                    let contents = '';
                    const data = exportsData[id];
                    const [imports, exports] = data;
                    if (!imports.length && !exports.length) {
                        // cjs
                        contents += `export default require("${relativePath}");`;
                    } else {
                        if (exports.includes('default')) {
                            contents += `import d from "${relativePath}";export default d;`;
                        }
                        if (
                            data.hasReExports ||
                            exports.length > 1 ||
                            exports[0] !== 'default'
                        ) {
                            contents += `\nexport * from "${relativePath}"`;
                        }
                    }

                    let ext = path.extname(relativePath).slice(1);
                    if (ext === 'mjs') {ext = 'js';}
                    return {
                        loader: ext,
                        contents,
                        resolveDir: root
                    };
                });

                build.onLoad(
                    { filter: /.*/, namespace: 'browser-external' },
                    ({ path: id }) => ({
                        contents:
                    `export default new Proxy({}, {
  get() {
    throw new Error('Module "${id}" has been externalized for ` +
                    `browser compatibility and cannot be accessed in client code.')
  }
})`
                    })
                );
            }
        };
    }
}
