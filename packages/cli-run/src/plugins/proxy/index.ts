/**
 * @Description: 代理服务启动入口
 * @author Beon
 * @date 2021/3/11
 */
import {Server} from './server';
import * as Koa from 'koa';
import '@vue/component-compiler-utils';
import * as http from 'http';
import {UtilsDec} from '../../decorators/utilsDec';
import {Config} from '../../types/config';
import CreateService from '../../server';

// 拓展descriptor;
declare module '@vue/component-compiler-utils' {
	interface SFCDescriptor {
		id: string
	}
}

type CreatServiceFn = () => CreateService

export class CreateProxyService {
	@UtilsDec.userConfig
	userConfig: Config
	port: number | string | boolean
	app: Koa
	defaultPort = 4000
	retryTimes = 10
	checkTimes = 0

	/**
	 * 构建函数
	 */
	constructor(CreateService: CreatServiceFn) {
	    if (this.userConfig?.server?.http2) {
	        this.app = (new Server()).app;
	        let port: number | string | boolean;
	        port = this.normalizePort(process.env.PORT || String(this.defaultPort));
	        this.portIsOccupied(port, CreateService);
	    } else {
		    CreateService();
	    }
	}

	/**
	 * 处理端口号
	 * @param {String} val 传入端口号
	 * @returns {string | boolean | number} 返回端口信息
	 */
	normalizePort(val: string): number {
	    const port = parseInt(val, 10);

	    if (isNaN(port)) {
	        // named pipe
	        return this.defaultPort;
	    }

	    if (port >= 0) {
	        // port number
	        this.port = port;
	        return port;
	    }
	    return this.defaultPort;
	}

	portIsOccupied (port: number, CreateService: CreatServiceFn): void {
	    if (this.retryTimes < this.checkTimes) {throw Error('代理端口被占用，请更改端口');}
	    this.checkTimes++;
	    // 创建服务并监听该端口
	    const server = http.createServer().listen(port);

	    server.on('listening', () => { // 执行这块代码说明端口未被占用
	        server.close(); // 关闭服务
	        const realServer = http.createServer(this.app.callback()).listen(port);
	        realServer.on('error', (err) => console.error(err));
		    UtilsDec.ProxyPort = port;
	        console.log('代理服务开启成功');
		    CreateService();
	    });

	    server.on('error', (err) => {
	        // @ts-ignore
	        if (err.code === 'EADDRINUSE') { // 端口已经被使用
	            console.log(`代理端口${port}已被占用，尝试为您切换端口`);
	            this.portIsOccupied(++port, CreateService);
	        }
	    });
	}
}
