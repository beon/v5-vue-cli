/**
 * @Description: koa服务配置
 * @author Beon
 * @date 2021/3/12
 */
import * as Koa from 'koa';
import {UtilsDec} from '../../decorators/utilsDec';
import {Config} from '../../types/config';
import * as proxy from 'koa-server-http-proxy';
import {Next} from 'koa';

export class Server {
	app: Koa
	@UtilsDec.userConfig
	userConfig: Config

	/**
	 * 构造函数
	 */
	constructor() {
	    this.app = new Koa();

	    this.app.use(Server.errorNew);

	    // 代理服务
	    this.userConfig?.server?.proxy && Object.keys(this.userConfig?.server?.proxy).forEach((context) => {
	        const options = this.userConfig?.server?.proxy?.[context];
	        options.headers?.host !== undefined || options.headers
	            ? Object.assign(options.headers, {host: ''})
	            : Object.assign(options, {headers: {host: ''}});
	        this.app.use(proxy(context, options));
	    });
	}

	/**
	 * 错误拦截处理
	 * @param ctx
	 * @param next
	 * @returns {Promise<void>}
	 */
	private static async errorNew(
	    ctx: Koa.Context,
	    next: Next
	): Promise<void> {
	    return next();
	}
}
