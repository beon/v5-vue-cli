const devUrl = 'https://10.11.4.12/'; // 2.0 测试环境
module.exports = {
    server: {
        proxy: {
            '/wcm(.*)': {
                target: devUrl,
                changOrigin: true
            },
            '/ids(.*)': {
                target: devUrl,
                changOrigin: true
            },
            '/api(.*)': {
                target: devUrl,
                changOrigin: true
            },
            // 舆情接口
            '/clue(.*)': {
                target: 'http://120.71.183.242:58223',
                changOrigin: true,
                pathRewrite: {
                    '^/clue': ''
                }
            },
            '/pm(.*)': {
                target: devUrl,
                changOrigin: true
            },
            '/yuegao(.*)': {
                target: devUrl,
                changOrigin: true
            }
        }
    }
};
