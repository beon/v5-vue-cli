# v5-vue-cli

## 介绍
> 本工具为vue项目脚手架运行工具

## 架构
> 使用ts + node编写，该项目为集合项目，具体在package中

## 推荐
> 现阶段只有第一个整合项目为快速开发脚手架，让你的webpack有vite一样的速度。
点我进入：https://gitee.com/beon/v5-vue-cli/tree/master/packages/cli-run

## 作者
> 本项目由 [Beon](https://juejin.cn/user/1662117312988695) 全程开发
